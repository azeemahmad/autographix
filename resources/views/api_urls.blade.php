@extends('layouts.master')
<style>
    .help-block{
        color:red !important;
    }
</style>
@section('content')
    <div class="row">
        <div class="col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    Example API urls
                </div>
                <div class="card-body no-padding">

                    <pre><code class="html"><b>Latest Catalog</b><br>http://www.viaviweb.in/envato/cc/catalog_app_demo/api.php?latest<br><br><b>Category List</b><br>http://www.viaviweb.in/envato/cc/catalog_app_demo/api.php?cat_list<br><br><b>Catalog list by Cat ID</b><br>http://www.viaviweb.in/envato/cc/catalog_app_demo/api.php?cat_id=1<br><br><b>Single Catalog</b><br>http://www.viaviweb.in/envato/cc/catalog_app_demo/api.php?catalog_id=1<br><br><b>App Details</b><br>http://www.viaviweb.in/envato/cc/catalog_app_demo/api.php</code></pre>

                </div>

            </div>
        </div>
    </div>
    <br/>
    <div class="clearfix"></div>
@endsection