<?php
$setting=App\Models\Setting::first();
?>
<aside class="app-sidebar" id="sidebar">
    <div class="sidebar-header"> <a class="sidebar-brand" href="{{url('/')}}"><img src="{{asset('images/'.$setting->app_logo)}}" alt="app logo" /></a>
        <button type="button" class="sidebar-toggle"> <i class="fa fa-times"></i> </button>
    </div>
    <div class="sidebar-menu">
        <ul class="sidebar-nav">
            <li <?php if(url()->current()==env('APP_URL').'/home'){?>class="active"<?php }?>> <a href="{{url('/home')}}">
                    <div class="icon"> <i class="fa fa-dashboard" aria-hidden="true"></i> </div>
                    <div class="title">Dashboard</div>
                </a>
            </li>
            <li <?php if(url()->current()==env('APP_URL').'/category' || url()->current()==env('APP_URL').'/category/create'){?>class="active"<?php }?>> <a href="{{url('/category')}}">
                    <div class="icon"> <i class="fa fa-sitemap" aria-hidden="true"></i> </div>
                    <div class="title">Categories</div>
                </a>
            </li>

            <li <?php if(url()->current()==env('APP_URL').'/catalog' || url()->current()==env('APP_URL').'/catalog/create'){?>class="active"<?php }?>> <a href="{{url('/catalog')}}">
                    <div class="icon"> <i class="fa fa-sticky-note" aria-hidden="true"></i> </div>
                    <div class="title">Catalog</div>
                </a>
            </li>


            <li <?php if(url()->current()==env('APP_URL').'/setting'){?>class="active"<?php }?>> <a href="{{url('/setting')}}">
                    <div class="icon"> <i class="fa fa-cog" aria-hidden="true"></i> </div>
                    <div class="title">Settings</div>
                </a>
            </li>

            <li <?php if(url()->current()==env('APP_URL').'/api_urls'){?>class="active"<?php }?>> <a href="{{url('/api_urls')}}">
                    <div class="icon"> <i class="fa fa-exchange" aria-hidden="true"></i> </div>
                    <div class="title">API URLS</div>
                </a>
            </li>

        </ul>
    </div>

</aside>