<!DOCTYPE html>
<html>
<head>
    <meta name="author" content="">
    <meta name="description" content="">
    <meta http-equiv="Content-Type"content="text/html;charset=UTF-8"/>
    <meta name="viewport"content="width=device-width, initial-scale=1.0">
    <title>{{env('APP_NAME')}}</title>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/vendor.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/flat-admin.css')}}">

    <!-- Theme -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/theme/blue-sky.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/theme/blue.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/theme/red.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/theme/yellow.css')}}">

    <script src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>

</head>
<body>
<div class="app app-default">
    @include('layouts.sidebar')
    <div class="app-container">
        @include('layouts.header')
        @yield('content')
        @include('layouts.footer')
    </div>
</div>
<script type="text/javascript" src="{{asset('assets/js/vendor.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/app.js')}}"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        $( ".datepicker" ).datepicker();
    } );
</script>
</body>
</html>