@extends('layouts.master')
<style>
    .help-block{
        color:red !important;
    }
</style>
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="page_title_block">
                <div class="col-md-5 col-xs-12">
                    <div class="page_title">Add Catalog</div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row mrg-top">
                <div class="col-md-12">
                    <div class="col-md-12 col-sm-12">
                        @if (session('flash_message'))
                            <span class="alert alert-success">
                           {{ session('flash_message') }}
                            </span>
                        @endif
                        @if (session('error_message'))
                            <span class="alert alert-danger">
                             {{ session('error_message') }}
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-body mrg_bottom">
                <form action="{{isset($catalog)?url('/catalog/'.$catalog->id):url('/catalog')}}"  method="post" class="form form-horizontal" enctype="multipart/form-data">
                    @if(isset($catalog))
                        {{ method_field('PATCH') }}
                    @endif
                    {{ csrf_field() }}
                    <div class="section">
                        <div class="section-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Category :-</label>
                                <div class="col-md-6">
                                    <select name="cat_id" id="cat_id" class="select2" >
                                        <option value="">--Select Category--</option>
                                        @foreach ($category as $optionKey => $optionValue)
                                            <option value="{{ $optionKey }}" {{ (isset($catalog->cat_id) && $catalog->cat_id == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first('cat_id', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Catalog Name :-</label>
                                <div class="col-md-6">
                                    <input type="text" name="catalog_name" id="catalog_name" value="{{isset($catalog->catalog_name)?$catalog->catalog_name:''}}" class="form-control" >
                                    {!! $errors->first('catalog_name', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Price :-</label>
                                <div class="col-md-6">
                                    <input type="text" name="catalog_price" id="catalog_price" value="{{isset($catalog->catalog_price)?$catalog->catalog_price:''}}" class="form-control" >
                                    {!! $errors->first('catalog_price', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Phone :-</label>
                                <div class="col-md-6">

                                    <input type="text" name="catalog_tel_no" id="catalog_tel_no" value="{{isset($catalog->catalog_tel_no)?$catalog->catalog_tel_no:''}}" class="form-control" >
                                    {!! $errors->first('catalog_tel_no', '<p class="help-block">:message</p>') !!}

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Email :-</label>
                                <div class="col-md-6">

                                    <input type="text" name="catalog_email" id="catalog_email" value="{{isset($catalog->catalog_email)?$catalog->catalog_email:''}}" class="form-control" >
                                    {!! $errors->first('catalog_email', '<p class="help-block">:message</p>') !!}

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Website :-</label>
                                <div class="col-md-6">

                                    <input type="text" name="catalog_wesite" id="catalog_wesite" value="{{isset($catalog->catalog_wesite)?$catalog->catalog_wesite:''}}" class="form-control" >
                                    {!! $errors->first('catalog_wesite', '<p class="help-block">:message</p>') !!}

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Address :-</label>
                                <div class="col-md-6">

                                    <textarea name="catalog_address" id="catalog_address" class="form-control">{{isset($catalog->catalog_address)?$catalog->catalog_address:''}}</textarea>
                                    {!! $errors->first('catalog_address', '<p class="help-block">:message</p>') !!}

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Latitude :-</label>
                                <div class="col-md-6">
                                    <input type="text" name="catalog_map_latitude" id="catalog_map_latitude" value="{{isset($catalog->catalog_map_latitude)?$catalog->catalog_map_latitude:''}}" class="form-control">
                                    {!! $errors->first('catalog_map_latitude', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Longitude :-</label>
                                <div class="col-md-6">
                                    <input type="text" name="catalog_map_longitude" id="catalog_map_longitude" value="{{isset($catalog->catalog_map_longitude)?$catalog->catalog_map_longitude:''}}" class="form-control">
                                    {!! $errors->first('catalog_map_longitude', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">&nbsp;</label>
                                <div class="col-md-6">
                                    Get Latitude and Longitude <a href="http://www.latlong.net" target="_blank">Here!</a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Featured Image :-</label>
                                <div class="col-md-6">
                                    <div class="fileupload_block">
                                        <input type="file" name="catalog_main_image" value="" id="fileupload">
                                        {!! $errors->first('catalog_main_image', '<p class="help-block">:message</p>') !!}
                                        @if(isset($catalog->catalog_main_image))
                                        <div class="fileupload_img"><img type="image" src="{{asset('images/'.$catalog->catalog_main_image)}}" alt="category image" /></div>
                                        @else
                                        <div class="fileupload_img"><img type="image" src="{{asset('assets/images/add-image.png')}}" alt="Featured image" /></div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Description :-</label>
                                <div class="col-md-6">

                                    <textarea name="catalog_desc" id="catalog_desc" class="form-control">{{isset($catalog->catalog_desc)?$catalog->catalog_desc:''}}</textarea>
                                    {!! $errors->first('catalog_desc', '<p class="help-block">:message</p>') !!}

                                    <script>CKEDITOR.replace( 'catalog_desc' );</script>
                                </div>
                            </div>
                            <div class="form-group">&nbsp;</div>
                            <div class="form-group" id="image_news">
                                <label class="col-md-3 control-label">Gallery Image :-</label>
                                <div class="col-md-6">
                                    <div class="fileupload_block">
                                        <input type="file" name="catalog_image[]" value="" id="fileupload" multiple>
                                        <div class="fileupload_img"><img type="image" src="{{asset('assets/images/add-image.png')}}" alt="Featured image" /></div>
                                        {!! $errors->first('catalog_image', '<p class="help-block">:message</p>') !!}

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">&nbsp;</label>
                                <div class="row">
                                    @if(isset($catlogImages) && $catlogImages->isNotEmpty())
                                        @foreach($catlogImages as $ik => $iv)
                                    <div class="col-md-1 col-sm-6">

                                        <img src="{{asset('images/'.$iv->catalog_image)}}" class="img-responsive">
                                        <a href="#">Delete</a>
                                    </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">&nbsp;</div>

                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" name="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection