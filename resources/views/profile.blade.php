@extends('layouts.master')
<style>
    .help-block{
        color:red !important;
    }
</style>
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="page_title_block">
                    <div class="col-md-5 col-xs-12">
                        <div class="page_title">Admin Profile</div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row mrg-top">
                    <div class="col-md-12">

                        <div class="col-md-12 col-sm-12">
                            @if (session('flash_message'))
                                <span class="alert alert-success">
                           {{ session('flash_message') }}
                            </span>
                            @endif
                            @if (session('error_password'))
                                <span class="alert alert-danger">
                             {{ session('error_password') }}
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="card-body mrg_bottom">

                    <form action="{{url('profile')}}" name="" method="post" class="form form-horizontal" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}
                        <div class="section">
                            <div class="section-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Profile Image :-</label>
                                    <div class="col-md-6">
                                        <div class="fileupload_block">
                                            <input type="file" name="image" id="fileupload">

                                            @if($user->image !="")
                                            <div class="fileupload_img"><img type="image" src="{{asset('images/'.$user->image)}}" alt="profile image" /></div>
                                            @else
                                            <div class="fileupload_img"><img type="image" src="{{asset('assets/images/add-image.png')}}" alt="add image" /></div>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Username :-</label>
                                    <div class="col-md-6">
                                        <input type="text" name="username" id="username" value="{{$user->username}}" class="form-control" required autocomplete="off">
                                        {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('old_password') ? 'has-error' : ''}}">
                                    <label for="old_password"
                                           class="col-md-3 control-label">{{ 'Old Password :-' }}</label>

                                    <div class="col-md-6">
                                        <input class="form-control" name="old_password" type="password"
                                               id="old_password">
                                        {!! $errors->first('old_password', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                                    <label for="password"
                                           class="col-md-3 control-label">{{ 'New Password :-' }}</label>

                                    <div class="col-md-6">
                                        <input class="form-control" name="password" type="password" id="password">
                                        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
                                    <label for="password_confirmation"
                                           class="col-md-3 control-label">{{ 'Password Confirmation :-' }}</label>

                                    <div class="col-md-6">
                                        <input class="form-control" name="password_confirmation" type="password"
                                               id="password_confirmation">
                                        {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Email :-</label>
                                    <div class="col-md-6">
                                        <input type="text" name="email" id="email" value="{{$user->email}}" class="form-control" required autocomplete="off">
                                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="submit" name="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection