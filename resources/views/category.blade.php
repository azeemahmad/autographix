@extends('layouts.master')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card mrg_bottom">
                <div class="page_title_block">
                    <div class="col-md-5 col-xs-12">
                        <div class="page_title">Manage Categories</div>
                    </div>
                    <div class="col-md-7 col-xs-12">
                        <div class="search_list">

                            <div class="add_btn_primary"><a href="{{url('category/create')}}">Add Category</a></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row mrg-top">
                    <div class="col-md-12">

                        <div class="col-md-12 col-sm-12">
                            @if (session('flash_message'))
                                <span class="alert alert-success">
                           {{ session('flash_message') }}
                            </span>
                            @endif
                            @if (session('error_message'))
                                <span class="alert alert-danger">
                             {{ session('error_message') }}
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mrg-top">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Category</th>
                            <th>Category Image</th>
                            <th class="cat_action_list">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($category as $key => $item)
                            <tr>
                                <td>{{$item->category_name}}</td>
                                <td><span class="category_img"><img
                                                src="images/thumbs/{{$item->category_image}}"/></span></td>
                                <td><a href="{{ url('/category/' . $item->cid . '/edit') }}" class="btn btn-primary">Edit</a>
                                    <form method="POST" action="{{ url('/category' . '/' . $item->cid) }}"
                                          accept-charset="UTF-8" style="display:inline">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-danger btn-xs" title="Delete Fund"
                                                onclick="return confirm(&quot;Are you sure you want to delete this category and related wallpaper?&quot;)"><i
                                                    class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>

@endsection
