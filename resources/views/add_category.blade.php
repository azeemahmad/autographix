@extends('layouts.master')
<style>
    .help-block{
        color:red !important;
    }
</style>
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="page_title_block">
                <div class="col-md-5 col-xs-12">
                    <div class="page_title"><?php if(isset($category)){?>Edit<?php }else{?>Add<?php }?> Category</div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row mrg-top">
                <div class="col-md-12">

                    <div class="col-md-12 col-sm-12">
                        @if (session('flash_message'))
                            <span class="alert alert-success">
                           {{ session('flash_message') }}
                            </span>
                        @endif
                        @if (session('error_message'))
                            <span class="alert alert-danger">
                             {{ session('error_message') }}
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-body mrg_bottom">
                <form action="{{isset($category)?url('/category/'.$category->cid):url('/category')}}" method="post" class="form form-horizontal" enctype="multipart/form-data">
                         @if(isset($category))
                        {{ method_field('PATCH') }}
                         @endif
                    {{ csrf_field() }}
                    <div class="section">
                        <div class="section-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Category Name :-</label>
                                <div class="col-md-6">
                                    <input type="text" name="category_name" id="category_name" value="{{isset($category->category_name)?$category->category_name:''}}" class="form-control">
                                    {!! $errors->first('category_name', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Select Image :-</label>
                                <div class="col-md-6">
                                    <div class="fileupload_block">
                                        <input type="file" name="category_image" value="fileupload" id="fileupload">
                                        @if(isset($category) && $category->category_image != "")
                                        <div class="fileupload_img"><img src="{{asset('images/'.$category->category_image)}}" alt="category image" /></div>
                                        @else
                                        <div class="fileupload_img"><img src="{{asset('assets/images/add-image.png')}}" alt="category image" /></div>
                                        @endif
                                        {!! $errors->first('category_image', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" name="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
