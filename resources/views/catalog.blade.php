@extends('layouts.master')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card mrg_bottom">
                <div class="page_title_block">
                    <div class="col-md-5 col-xs-12">
                        <div class="page_title">Manage Catalog</div>
                    </div>
                    <div class="col-md-7 col-xs-12">
                        <div class="search_list">

                            <div class="add_btn_primary"> <a href="{{url('catalog/create')}}">Add Catalog</a> </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row mrg-top">
                    <div class="col-md-12">

                        <div class="col-md-12 col-sm-12">
                            @if (session('flash_message'))
                                <span class="alert alert-success">
                           {{ session('flash_message') }}
                            </span>
                            @endif
                            @if (session('error_message'))
                                <span class="alert alert-danger">
                             {{ session('error_message') }}
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mrg-top">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Category</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th class="cat_action_list">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($catalog as $key => $item)
                        <tr>
                            <td>{{isset($item->category->category_name)?$item->category->category_name:''}}</td>
                            <td>{{stripslashes($item->catalog_name)}}</td>
                            <td>
                                @if($item->status != "0")
                                <a href="#" title="Change Status"><span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span>Enable</span></span></a>
                                @else
                                <a href="#" title="Change Status"><span class="badge badge-danger badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span>Disable </span></span></a>
                               @endif
                            </td>
                            <td><a href="{{ url('/catalog/' . $item->id . '/edit')}}" class="btn btn-primary">Edit</a>
                                <form method="POST" action="{{ url('/catalog' . '/' . $item->id) }}"
                                      accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-xs" title="Delete Fund"
                                            onclick="return confirm(&quot;Are you sure you want to delete this catalog?&quot;)"><i
                                                class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                    </button>
                                </form>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination-wrapper pull-right"> {!! $catalog->appends($_GET)->render() !!} </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
