@extends('layouts.master')
@section('content')
    <div class="btn-floating" id="help-actions">
        <div class="btn-bg"></div>
        <button type="button" class="btn btn-default btn-toggle" data-toggle="toggle" data-target="#help-actions"> <i class="icon fa fa-plus"></i> <span class="help-text">Shortcut</span> </button>
        <div class="toggle-content">
            <ul class="actions">
                <li><a href="http://www.viaviweb.com" target="_blank">Website</a></li>
                <li><a href="https://codecanyon.net/user/viaviwebtech?ref=viaviwebtech" target="_blank">About</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12"> <a href="{{url('/category')}}" class="card card-banner card-green-light">
                <div class="card-body"> <i class="icon fa fa-sitemap fa-4x"></i>
                    <div class="content">
                        <div class="title">Categories</div>
                        <div class="value"><span class="sign"></span>{{$total_category}}</div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12"> <a href="{{url('/catalog')}}" class="card card-banner card-yellow-light">
                <div class="card-body"> <i class="icon fa fa-sticky-note fa-4x"></i>
                    <div class="content">
                        <div class="title">Catalog</div>
                        <div class="value"><span class="sign"></span>{{$total_catalog}}</div>
                    </div>
                </div>
            </a>
        </div>
    </div>
@endsection
