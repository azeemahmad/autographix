@extends('layouts.master')
<style>
    .help-block{
        color:red !important;
    }
</style>
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="page_title_block">
                    <div class="col-md-5 col-xs-12">
                        <div class="page_title">Settings</div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row mrg-top">
                    <div class="col-md-12">

                        <div class="col-md-12 col-sm-12">
                            @if (session('flash_message'))
                                <span class="alert alert-success">
                           {{ session('flash_message') }}
                            </span>
                            @endif
                            @if (session('error_message'))
                                <span class="alert alert-danger">
                             {{ session('error_message') }}
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="card-body mrg_bottom">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#app_settings" aria-controls="app_settings" role="tab" data-toggle="tab">App Settings</a></li>
                        <li role="presentation"><a href="#api_settings" aria-controls="api_settings" role="tab" data-toggle="tab">API Settings</a></li>
                        <li role="presentation"><a href="#api_privacy_policy" aria-controls="api_privacy_policy" role="tab" data-toggle="tab">App Privacy Policy</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="app_settings">
                            <form action="{{url('app_setting/'.$settings_row->id)}}" name="" method="post" class="form form-horizontal" enctype="multipart/form-data">
                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}
                                <div class="section">
                                    <div class="section-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">App Name :-</label>
                                            <div class="col-md-6">
                                                <input type="text" name="app_name" id="app_name" value="<?php echo stripslashes($settings_row['app_name']);?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">App Logo :-</label>
                                            <div class="col-md-6">
                                                <div class="fileupload_block">
                                                    <input type="file" name="app_logo" id="fileupload">
                                                    <?php if($settings_row['app_logo']!="") {?>
                                                    <div class="fileupload_img"><img type="image" src="{{'images/'.$settings_row->app_logo}}" alt="image" /></div>
                                                    <?php } else {?>
                                                    <div class="fileupload_img"><img type="image" src="{{asset('assets/images/add-image.png')}}" alt="image" /></div>
                                                    <?php }?>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">App Description :-</label>
                                            <div class="col-md-6">

                                                <textarea name="app_description" id="app_description" class="form-control"><?php echo stripslashes($settings_row['app_description']);?></textarea>

                                                <script>CKEDITOR.replace( 'app_description' );</script>
                                            </div>
                                        </div>
                                        <div class="form-group">&nbsp;</div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">App Version :-</label>
                                            <div class="col-md-6">
                                                <input type="text" name="app_version" id="app_version" value="<?php echo $settings_row['app_version'];?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Author :-</label>
                                            <div class="col-md-6">
                                                <input type="text" name="app_author" id="app_author" value="<?php echo $settings_row['app_author'];?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Contact :-</label>
                                            <div class="col-md-6">
                                                <input type="text" name="app_contact" id="app_contact" value="<?php echo $settings_row['app_contact'];?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Email :-</label>
                                            <div class="col-md-6">
                                                <input type="text" name="app_email" id="app_email" value="<?php echo $settings_row['app_email'];?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Website :-</label>
                                            <div class="col-md-6">
                                                <input type="text" name="app_website" id="app_website" value="<?php echo $settings_row['app_website'];?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Developed By :-</label>
                                            <div class="col-md-6">
                                                <input type="text" name="app_developed_by" id="app_developed_by" value="<?php echo $settings_row['app_developed_by'];?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-9 col-md-offset-3">
                                                <button type="submit" name="submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="api_settings">
                            <form action="{{url('api_setting/'.$settings_row->id)}}" name="" method="post" class="form form-horizontal" enctype="multipart/form-data">
                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}
                                <div class="section">
                                    <div class="section-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Latest Limit:-</label>
                                            <div class="col-md-6">

                                                <input type="number" name="api_latest_limit" id="api_latest_limit" value="<?php echo $settings_row['api_latest_limit'];?>" class="form-control">
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Category List Order By:-</label>
                                            <div class="col-md-6">


                                                <select name="api_cat_order_by" id="api_cat_order_by" class="select2">
                                                    <option value="cid" <?php if($settings_row['api_cat_order_by']=='cid'){?>selected<?php }?>>ID</option>
                                                    <option value="category_name" <?php if($settings_row['api_cat_order_by']=='category_name'){?>selected<?php }?>>Name</option>

                                                </select>

                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Category Video Order:-</label>
                                            <div class="col-md-6">


                                                <select name="api_cat_post_order_by" id="api_cat_post_order_by" class="select2">
                                                    <option value="ASC" <?php if($settings_row['api_cat_post_order_by']=='ASC'){?>selected<?php }?>>ASC</option>
                                                    <option value="DESC" <?php if($settings_row['api_cat_post_order_by']=='DESC'){?>selected<?php }?>>DESC</option>

                                                </select>

                                            </div>

                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-9 col-md-offset-3">
                                                <button type="submit" name="api_submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="api_privacy_policy">
                            <form action="{{url('api_privacy_policy/'.$settings_row->id)}}" name="" method="post" class="form form-horizontal" enctype="multipart/form-data">
                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}
                                <div class="section">
                                    <div class="section-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">App Privacy Policy :-</label>
                                            <div class="col-md-6">

                                                <textarea name="app_privacy_policy" id="privacy_policy" class="form-control"><?php echo $settings_row['app_privacy_policy'];?></textarea>

                                                <script>CKEDITOR.replace( 'privacy_policy' );</script>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-9 col-md-offset-3">
                                                <button type="submit" name="app_pri_poly" class="btn btn-primary">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection