<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::group(['middleware' => 'guest'], function () {
    Route::get('/', function () {
        return view('welcome');
    });
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@postlogin');
});
Route::group(['middleware' => 'auth'], function () {
    Route::get('/logout', 'Auth\LoginController@logout')->name('admin.logout');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/profile', 'HomeController@profile')->name('profile');
    Route::patch('/profile', 'HomeController@saveprofile')->name('saveprofile');
    Route::resource('/category', 'CategoryController');
    Route::resource('/catalog', 'CatalogController');
    Route::get('/setting', 'SettingController@index');
    Route::patch('app_setting/{id}','SettingController@app_setting');
    Route::patch('api_setting/{id}','SettingController@api_setting');
    Route::patch('api_privacy_policy/{id}','SettingController@api_privacy_policy');
    Route::resource('/api_urls', 'ApiUrlsController');
});


