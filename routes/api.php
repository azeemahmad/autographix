<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'client', 'namespace' => 'Client\Auth'], function () {
    Route::post('/mobile_verification','ClientController@mobile_verification');
    Route::post('/otp_verification','ClientController@otp_verification');
    Route::post('/register','ClientController@register');
    Route::post('/send_notification','ClientController@send_notification');
    Route::post('/accountdetails','ClientController@get_account_details');
    Route::post('/updateaccountdetails','ClientController@update_account_details');
});
Route::post('/get_closed_card_details', 'MainController@get_closed_card_details');
Route::post('/redeem_card', 'MainController@redeem_card');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
