<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");

	require_once("thumbnail_images.class.php");

  //Get Category
	$cat_qry="SELECT * FROM tbl_category ORDER BY category_name";
	$cat_result=mysqli_query($mysqli,$cat_qry); 
 

  if(isset($_GET['catalog_id']))
  {
       
      $qry="SELECT * FROM tbl_catalog where id='".$_GET['catalog_id']."'";
      $result=mysqli_query($mysqli,$qry);
      $row=mysqli_fetch_assoc($result);

      //Gallery Images
      $qry1="SELECT * FROM tbl_catalog_image where catalog_id='".$_GET['catalog_id']."'";
      $result1=mysqli_query($mysqli,$qry1);
       
  }
	
	if(isset($_POST['submit']))
	{ 
        
            if($_FILES['catalog_main_image']['name']!="")
            {
              $catalog_main_image=rand(0,99999)."_".$_FILES['catalog_main_image']['name'];
       
              //Main Image
              $tpath1='images/'.$catalog_main_image;        
              $pic1=compress_image($_FILES["catalog_main_image"]["tmp_name"], $tpath1, 80);
         
             
                  $data = array( 
                 'cat_id'  =>  $_POST['cat_id'],
                 'catalog_name'  =>  addslashes($_POST['catalog_name']),
                 'catalog_price'  =>  $_POST['catalog_price'],
                 'catalog_address'  =>  addslashes($_POST['catalog_address']),
                 'catalog_tel_no'  =>  $_POST['catalog_tel_no'],
                 'catalog_email'  =>  $_POST['catalog_email'],
                 'catalog_wesite'  =>  $_POST['catalog_wesite'],
                 'catalog_map_latitude'  =>  $_POST['catalog_map_latitude'],
                 'catalog_map_longitude'  =>  $_POST['catalog_map_longitude'],
                 'catalog_main_image'  =>  $catalog_main_image,           
                 'catalog_desc'  =>  addslashes($_POST['catalog_desc'])
                  );    

            }
            else
            {
                    $data = array( 
                   'cat_id'  =>  $_POST['cat_id'],
                   'catalog_name'  =>  addslashes($_POST['catalog_name']),
                   'catalog_price'  =>  $_POST['catalog_price'],
                   'catalog_address'  =>  addslashes($_POST['catalog_address']),
                   'catalog_tel_no'  =>  $_POST['catalog_tel_no'],
                   'catalog_email'  =>  $_POST['catalog_email'],
                   'catalog_wesite'  =>  $_POST['catalog_wesite'],
                   'catalog_map_latitude'  =>  $_POST['catalog_map_latitude'],
                   'catalog_map_longitude'  =>  $_POST['catalog_map_longitude'],
                   'catalog_desc'  =>  addslashes($_POST['catalog_desc'])
                    );
            }
        
           
    $qry=Update('tbl_catalog', $data, "WHERE id = '".$_POST['catalog_id']."'");

     $catalog_id=$_POST['catalog_id'];

    $size_sum = array_sum($_FILES['catalog_image']['size']);
     
  if($size_sum > 0)
   { 
      for ($i = 0; $i < count($_FILES['catalog_image']['name']); $i++) 
      {
           $file_name= str_replace(" ","-",$_FILES['catalog_image']['name'][$i]);
             
           $catalog_image=rand(0,99999)."_".$file_name;
         
           //Main Image
           $tpath1='images/'.$catalog_image;       
           $pic1=compress_image($_FILES["catalog_image"]["tmp_name"][$i], $tpath1, 80);

            $data1 = array(
                'catalog_id'=>$catalog_id,
                'catalog_image'  => $catalog_image                         
                );      

            $qry1 = Insert('tbl_catalog_image',$data1); 

      }
    }
         
		$_SESSION['msg']="11"; 
		header( "Location:edit_catalog.php?catalog_id=".$_POST['catalog_id']);
		exit;	

		 
	}
	

  //Delete gallery image
  if(isset($_GET['image_id']))
  {
      $img_rss=mysqli_query($mysqli,'SELECT * FROM tbl_catalog_image WHERE img_id=\''.$_GET['image_id'].'\'');
      $img_rss_row=mysqli_fetch_assoc($img_rss);
      
      if($img_rss_row['catalog_image']!="")
      {
          unlink('images/'.$img_rss_row['catalog_image']);
           
      }
  
    Delete('tbl_catalog_image','img_id='.$_GET['image_id'].'');
    
    
    header( "Location:edit_catalog.php?catalog_id=".$_GET['catalog_id']);
    exit; 
  }
	  
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
 

<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="page_title_block">
            <div class="col-md-5 col-xs-12">
              <div class="page_title">Edit Catalog</div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row mrg-top">
            <div class="col-md-12">
               
              <div class="col-md-12 col-sm-12">
                <?php if(isset($_SESSION['msg'])){?> 
                 <div class="alert alert-success alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                  <?php echo $client_lang[$_SESSION['msg']] ; ?></a> </div>
                <?php unset($_SESSION['msg']);}?> 
              </div>
            </div>
          </div>
          <div class="card-body mrg_bottom"> 
            <form action="" name="addeditcategory" method="post" class="form form-horizontal" enctype="multipart/form-data">
                            <input  type="hidden" name="catalog_id" value="<?php echo $_GET['catalog_id'];?>" />

              <div class="section">
                <div class="section-body">
                 
                  
                  <div class="form-group">
                    <label class="col-md-3 control-label">Category :-</label>
                    <div class="col-md-6">
                      <select name="cat_id" id="cat_id" class="select2" required>
                        <option value="">--Select Category--</option>
                        <?php
                            while($cat_row=mysqli_fetch_array($cat_result))
                            {
                        ?>                       
                        <option value="<?php echo $cat_row['cid'];?>" <?php if($cat_row['cid']==$row['cat_id']){?>selected<?php }?>><?php echo $cat_row['category_name'];?></option>                           
                        <?php
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Catalog Name :-</label>
                    <div class="col-md-6">
                      <input type="text" name="catalog_name" id="catalog_name" value="<?php echo stripslashes($row['catalog_name']);?>" class="form-control" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Price :-</label>
                    <div class="col-md-6">
                      <input type="text" name="catalog_price" id="catalog_price" value="<?php echo $row['catalog_price'];?>" class="form-control" required>
                    </div>
                  </div>                  
                  <div class="form-group">
                    <label class="col-md-3 control-label">Phone :-</label>
                    <div class="col-md-6">
                 
                      <input type="text" name="catalog_tel_no" id="catalog_tel_no" value="<?php echo $row['catalog_tel_no'];?>" class="form-control" required>

                     </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Email :-</label>
                    <div class="col-md-6">
                 
                      <input type="text" name="catalog_email" id="catalog_email" value="<?php echo $row['catalog_email'];?>" class="form-control" required>

                     </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Website :-</label>
                    <div class="col-md-6">
                 
                      <input type="text" name="catalog_wesite" id="catalog_wesite" value="<?php echo $row['catalog_wesite'];?>" class="form-control" required>

                     </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Address :-</label>
                    <div class="col-md-6">
                 
                      <textarea name="catalog_address" id="catalog_address" class="form-control"><?php echo stripslashes($row['catalog_address']);?></textarea>

                     </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Latitude :-</label>
                    <div class="col-md-6">
                      <input type="text" name="catalog_map_latitude" id="catalog_map_latitude" value="<?php echo $row['catalog_map_latitude'];?>" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Longitude :-</label>
                    <div class="col-md-6">
                      <input type="text" name="catalog_map_longitude" id="catalog_map_longitude" value="<?php echo $row['catalog_map_longitude'];?>" class="form-control">
                    </div>
                  </div>
                   <div class="form-group">
                    <label class="col-md-3 control-label">&nbsp;</label>
                    <div class="col-md-6">
                      Get Latitude and Longitude <a href="http://www.latlong.net" target="_blank">Here!</a>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Featured Image :-</label>
                    <div class="col-md-6">
                      <div class="fileupload_block">
                        <input type="file" name="catalog_main_image" value="" id="fileupload">
                            
                            <?php if($row['catalog_main_image']!="") {?>
                            <div class="fileupload_img"><img type="image" src="images/<?php echo $row['catalog_main_image'];?>" alt="category image" /></div>
                          <?php } else {?>
                            <div class="fileupload_img"><img type="image" src="assets/images/add-image.png" alt="Featured image" /></div>
                          <?php }?>
                           
                      </div>
                    </div>
                  </div>                   
                  <div class="form-group">
                    <label class="col-md-3 control-label">Description :-</label>
                    <div class="col-md-6">
                 
                      <textarea name="catalog_desc" id="catalog_desc" class="form-control"><?php echo stripslashes($row['catalog_desc']);?></textarea>

                      <script>CKEDITOR.replace( 'catalog_desc' );</script>
                    </div>
                  </div>                   
                  <div class="form-group">&nbsp;</div>
                   <div class="form-group" id="image_news">
                    <label class="col-md-3 control-label">Gallery Image :-</label>
                    <div class="col-md-6">
                      <div class="fileupload_block">
                        <input type="file" name="catalog_image[]" value="" id="fileupload" multiple>
                            
                            <div class="fileupload_img"><img type="image" src="assets/images/add-image.png" alt="Featured image" /></div>
                           
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                  <label class="col-md-3 control-label">&nbsp;</label>
                      <div class="row">
                          <?php
                            while ($row_img=mysqli_fetch_array($result1)) {?>
                               <div class="col-md-1 col-sm-6">
                          
                            <img src="images/<?php echo $row_img['catalog_image'];?>" class="img-responsive">
                            <a href="edit_catalog.php?image_id=<?php echo $row_img['img_id'];?>&catalog_id=<?php echo $_GET['catalog_id'];?>">Delete</a>
                           
                        </div>
                            <?php
                          }
                          ?>
                         
       
                     </div>
                  </div>
                  <div class="form-group">&nbsp;</div>
                  <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
        
<?php include("includes/footer.php");?>       
