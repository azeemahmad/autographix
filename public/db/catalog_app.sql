-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 11, 2017 at 07:52 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `catalog_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `username`, `password`, `email`, `image`) VALUES
(1, 'admin', 'admin', 'viaviwebtech@gmail.com', 'profile.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_catalog`
--

CREATE TABLE `tbl_catalog` (
  `id` int(11) NOT NULL,
  `cat_id` varchar(255) NOT NULL,
  `catalog_name` varchar(255) NOT NULL,
  `catalog_price` varchar(255) NOT NULL,
  `catalog_address` varchar(255) NOT NULL,
  `catalog_tel_no` varchar(255) NOT NULL,
  `catalog_email` varchar(255) NOT NULL,
  `catalog_wesite` varchar(255) NOT NULL,
  `catalog_map_latitude` varchar(255) NOT NULL,
  `catalog_map_longitude` varchar(255) NOT NULL,
  `catalog_main_image` varchar(255) NOT NULL,
  `catalog_desc` text NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_catalog`
--

INSERT INTO `tbl_catalog` (`id`, `cat_id`, `catalog_name`, `catalog_price`, `catalog_address`, `catalog_tel_no`, `catalog_email`, `catalog_wesite`, `catalog_map_latitude`, `catalog_map_longitude`, `catalog_main_image`, `catalog_desc`, `status`) VALUES
(3, '2', 'Mi A1 (Black, 64 GB)  (4 GB RAM)', '14999', 'India', '9999999999', 'fsas@gmail.com', 'mi.com/in', '33.634454', '121.634454', '49869_mi-mi-a1-na-original-imaexg9njdddfphr.jpeg', '<ul>\r\n	<li>4 GB RAM | 64 GB ROM | Expandable Upto 128 GB</li>\r\n	<li>5.5 inch Full HD Display</li>\r\n	<li>12MP + 12MP Dual Rear Camera | 5MP Front Camera</li>\r\n	<li>3080 mAh Li-polymer Battery</li>\r\n	<li>Qualcomm Snapdragon 625 64 bit Octa Core 2GHz Processor</li>\r\n	<li>Android Nougat 7.1.2 | Stock Android Version</li>\r\n	<li>Android One Smartphone - with confirmed upgrades to Android Oreo and Android P</li>\r\n</ul>\r\n\r\n<p>Easy Payment Options</p>\r\n\r\n<ul>\r\n	<li>No cost EMI starting from ₹1,667/month</li>\r\n	<li>Cash on Delivery</li>\r\n	<li>Net banking &amp; Credit/ Debit/ ATM card</li>\r\n</ul>\r\n', '1'),
(4, '2', 'OnePlus 5', '33,000/-', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.amazon.in/OnePlus-Slate-Gray-64GB-memory/dp/B01NAKTR2H/ref=sr_1_1?s=electronics&ie=UTF8&qid=1510312281&sr=1-1&keywords=one+plus+5', '33.634454', '121.634454', '14313_OnePlus5.jpg', '<ul>\r\n	<li>20MP+16MP primary dual camera and 16MP front facing camera</li>\r\n	<li>13.97 centimeters (5.5-inch) capacitive touchscreen FHD display with 1920 x 1080 pixels resolution. Cover Glass:2.5D Corning Gorilla Glass 5</li>\r\n	<li>Android v7.1.1 Nougat OS with Qualcomm Snapdragon 835 octa core processor</li>\r\n	<li>6 GB RAM, 64 GB internal memory and dual nano SIM dual-standby (4G+4G)</li>\r\n	<li>3300mAH lithium Polymer battery with Dash Charge technology</li>\r\n	<li>Fingerprint scanner, all-metal unibody and NFC enabled</li>\r\n	<li>1 year manufacturer warranty for device and 6 months manufacturer warranty for in-box accessories including batteries from the date of purchase</li>\r\n</ul>\r\n', '1'),
(5, '2', 'Nokia 6', '14,999/-', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.amazon.in/Nokia-6-Matte-Black-32GB/dp/B072LNVPMN/ref=sr_1_1?s=electronics&ie=UTF8&qid=1510312039&sr=1-1&keywords=nokia+6', '33.634454', '121.634454', '12627_Nokia6_1.jpg', '<ul>\r\n	<li>16MP PDAF primary camera and 8MP AF front facing camera</li>\r\n	<li>13.97 centimeters (5.5-inch)&nbsp; FHD display with 1920 x 1080 pixels resolution</li>\r\n	<li>Android v7.1.1 Nougat OS with Qualcomm Snapdragon 430 octa core processor</li>\r\n	<li>3 GB RAM, 32 GB internal memory and dual-standby (4G+4G) with Micro SD card hybrid support</li>\r\n	<li>3000mAH lithium-ion battery</li>\r\n	<li>Fingerprint scanner, all-metal unibody and NFC enabled, Dual Speakers with dedicated amplifier and Dolby Digital Sound</li>\r\n	<li>1 year manufacturer warranty for device and 6 months manufacturer warranty for in-box accessories including batteries from the date of purchase</li>\r\n</ul>\r\n', '1'),
(6, '2', 'Vivo V5Plus', '19,990/-', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'www.xyz.com', 'https://www.amazon.in/gp/product/B06Y1PL4XV/ref=s9_acsd_cdeal_hd_bw_b1W1mg3_c_x_w?pf_rd_m=A1VBAL9TL5WCBF&pf_rd_s=merchandised-search-5&pf_rd_r=2Q3W78YZ6ZBMV2VMTABJ&pf_rd_t=101&pf_rd_p=2c3dc16a-1091-5882-ac4b-30e57d655592&pf_rd_i=1389401031', '33.634454', '121.634454', '19634_VivoV5Plus.jpg', '<ul>\r\n	<li>With no cost EMI option on credit cards during check out</li>\r\n	<li>16MP primary camera and 20MP front facing camera</li>\r\n	<li>5.5inch FHD IPS In-cell capacitive touchscreen with 1920 x 1080 pixels resolution and 401 ppi pixel density</li>\r\n	<li>Android v6.0 Marshmallow operating system with 2.0GHz Qualcomm Snapdragon 625 octa core processor, 4GB RAM, 64GB internal memory and dual SIM (nano+nano) dual-standby (4G+4G)</li>\r\n	<li>3055mAH lithium_ion battery</li>\r\n	<li>1 year manufacturer warranty for device and 6 months manufacturer warranty for in-box accessories including batteries from the date of purchase</li>\r\n</ul>\r\n', '1'),
(7, '9', 'aston-martin-db11', '13999282.35 - 15195105.98', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.caranddriver.com/aston-martin/db11', '33.634454', '121.634454', '35972_aston-martin-db11.jpg', '<p>Turning up its looks and performance literally to 11, the DB11 continues Aston Martin&rsquo;s tradition of blending style and power. Two twin-turbo engines are offered&mdash;a 503-hp 4.0-liter V-8 and a 600-hp 5.2-liter V-12&mdash;both teamed with a paddle-shifted eight-speed automatic. In our testing, the V-12 launched the DB11 from zero to 60 mph in 3.6 seconds. A lightweight structure, torque-vectoring system, and stiff chassis result in impressively agile handling. Want to feel like 007? This is your car.</p>\r\n', '1'),
(8, '9', 'BMW-i8', '2.14 Cr*', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.cardekho.com/carmodels/BMW/BMW_i8', '33.634454', '121.634454', '52030_BMW-i8.jpg', '<p>Three pistons combusting internally, a dash of electrons, and lots of aluminum and carbon fiber are the i8&rsquo;s main ingredients. Its 357-hp hybrid powertrain drives all four wheels; in our hands, the i8 hit 60 mph in 4.0 seconds and saw 36 MPGe on our 200-mile test run. The poised chassis and sharp steering are good fun, but eco-friendly tires and regenerative braking hinder performance. Familiar controls nestle in a plush cabin; the i8&rsquo;s eye-catching styling is straight out of a sci-fi flick.</p>\r\n', '1'),
(9, '9', 'Lamborghini-Aventador', '4.84 - 5.79 Cr*', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.cardekho.com/carmodels/Lamborghini/Lamborghini_Aventador', '33.634454', '121.634454', '58223_Lamborghini-Aventador.jpg', '<p>Brutally powerful and obscenely flamboyant, the Aventador is unburdened by reality. Crazy expensive and crazy fast, it&rsquo;s capable of amazing performance without feeling like it&rsquo;s going to spin out into a ditch, which is refreshing in a supercar. Available as a coupe (for now), it has a 6.5-liter 730-hp V-12, a 7-speed automated manual transmission and all-wheel drive. For the ultimate, the Superveloce has 740 hp and a claimed top speed of 217 mph. In our testing, it did 0-60 mph in 2.7 seconds.</p>\r\n', '1'),
(10, '3', 'U.S. Polo Denim Co. Men\'s Cotton Sweatshirt', '1,648.00 - 1,978.00', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.amazon.in/US-Polo-Sweatshirt-8907259140018_USSS0513_L-_Honeygold/dp/B013F00XUS/ref=sr_1_26?s=apparel&rps=1&ie=UTF8&qid=1510315870&sr=1-26&nodeID=1968024031&psd=1&refinements=p_98%3A10440597031', '33.634454', '121.634454', '12252_USPOLO_1.jpg', '<ul>\r\n	<li>100% Cotton</li>\r\n	<li>Standard</li>\r\n	<li>Front open hooded</li>\r\n	<li>Cold wash</li>\r\n	<li>Made in India</li>\r\n	<li>This product is brought to you by USPA Denim Company which is an authorized sub-brand of U.S. Polo Assn.</li>\r\n</ul>\r\n', '1'),
(11, '3', 'Label Ritu Kumar Black Floor Length Dress', '2,835.00', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.amazon.in/Label-Ritu-Kumar-Black-Length/dp/B071H7X9K3/ref=sr_1_44?s=apparel&ie=UTF8&qid=1510317319&sr=1-44&nodeID=1968445031&psd=1', '33.634454', '121.634454', '88089_RituKumar_casual_dress1.jpg', '<ul>\r\n	<li>55 % Viscose and 45% georgette</li>\r\n	<li>Professional dry clean</li>\r\n	<li>Peplum</li>\r\n	<li>Sleeveless</li>\r\n	<li>Maxi</li>\r\n	<li>Garment front length Is 58 Inches</li>\r\n</ul>\r\n', '1'),
(12, '3', 'Marie Claire Women\\\'s Fit and Flare Maroon Dress', '1,448', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.flipkart.com/marie-claire-women-s-fit-flare-maroon-dress/p/itmesfasqzvrrywj?pid=DREESFEERNQMFGRH&srno=b_3_3&otracker=browse&lid=LSTDREESFEERNQMFGRHZX5S17&fm=organic&iid=82ef3f4c-3911-44c2-a8e0-5bd784fa04f1.DREESFEERNQMFGRH.SEARCH', '33.634454', '121.634454', '78998_Marie_Dress.jpeg', '<p>Maintain a trendy style all year long with this stunning dress from Marie Claire. Match this solid burgundy piece with an incredible statement bag and high heels to maximise your next lunch look.</p>\r\n\r\n<p>Ideal For: Women&#39;s<br />\r\nType: Fit and Flare<br />\r\nStyle Code: MC10248A<br />\r\nSuitable For: Western Wear<br />\r\nPack of: 1<br />\r\nNeck: Round Neck<br />\r\nFabric Care: Machine wash as per tag</p>\r\n', '1'),
(13, '6', 'LG 108 cm (43 inches) 43LH576T Full HD Smart ', '44,790.00', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.amazon.in/LG-inches-43LH576T-Smart-Black/dp/B01GNYE0X4/ref=lp_7198570031_1_8?s=electronics&ie=UTF8&qid=1510378189&sr=1-8', '', '', '89321_LGLED.jpg', '<ul>\r\n	<li>108 centimeters LED Full HD 1920 x 1080</li>\r\n	<li>Connectivity HDMI: 1, USB Port: 1</li>\r\n	<li>Warranty Information: 1 year warranty provided by the manufacturer from date of purchase</li>\r\n	<li>For requesting installation, Amazon will connect with you via Email and SMS on your registered mobile number to share the next steps for Installation. In case you need any assistance, please reach out to us on our Toll Free Number 1800 3000 9027.</li>\r\n	<li>Easy Returns: This product is eligible for full refund within 10 days of delivery in case of any product defects, damage or features not matching the description provided</li>\r\n</ul>\r\n', '1'),
(15, '6', 'Samsung 545 L Frost-free Side-by-Side Refrigerator (RSA1SHMG1/TL, Metal Graphite)', '71,390.00', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.amazon.in/Samsung-Frost-free-Refrigerator-RSA1SHMG1-Graphite/dp/B01BD7PRWY/ref=sr_1_5?s=kitchen&rps=1&ie=UTF8&qid=1510378498&sr=1-5&refinements=p_98%3A10440597031%2Cp_n_feature_thirteen_browse-bin%3A2753039031&dpID=31vWaAMSCeL&preST=_SY445_QL7', '', '', '36085_Samsung545L.jpg', '<ul>\r\n	<li>Installation: For requesting an installation/demo for this product once delivered, please call Samsung support directly on 1800 40 7267864 and provide the product&#39;s model name.</li>\r\n	<li>Frost-free side-by-side refrigerator, 545 litres capacity</li>\r\n	<li>1 year warranty on product ; 10 years warranty on compressor</li>\r\n	<li>Multi flow, door alarm, digital inverter technology</li>\r\n	<li>Digital display child lock, power cool, power freeze</li>\r\n	<li>Slide out Drawers (FRE2 / REF 2)</li>\r\n	<li>5 Smart Sensore, Dairy Bin</li>\r\n</ul>\r\n', '1'),
(16, '6', 'LG 8 kg Fully-Automatic Front Loading Washing ', '42,990.00', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.amazon.in/LG-Fully-Automatic-Loading-Washing-FH4G6TDNL42/dp/B071NFC4NK/ref=sr_1_16?s=kitchen&rps=1&ie=UTF8&qid=1510378802&sr=1-16&refinements=p_98%3A10440597031%2Cp_89%3ALG', '', '', '25584_LG8KGW.jpg', '<ul>\r\n	<li>Fully-automatic front-loading washing machine</li>\r\n	<li>8 kg capacity</li>\r\n	<li>Chrome door and 1400 rpm</li>\r\n	<li>Warranty: 2 years on product and 10 years on motor</li>\r\n	<li>6 motion DD and smart diagnosis</li>\r\n</ul>\r\n', '1'),
(17, '6', 'Glen Kitchen Chimney GL 6062 TS Touch Control', '30,540.00', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.amazon.in/Glen-Kitchen-Chimney-Control-1250m3/dp/B01J9Q3QZQ/ref=sr_1_10?s=kitchen&rps=1&ie=UTF8&qid=1510379011&sr=1-10&refinements=p_98%3A10440597031%2Cp_36%3A2000000-99999999', '', '', '47679_Chimneys.jpg', '<ul>\r\n	<li>Chimney Features :- A sleek contemporary hood cast in glass &amp; steel Toughened glass front panel Single SS tube Baffle filter Size 60 Cms New flame retardant plastic housing Timer function Switches off automatically after sometime Sleek touch sensor control panel Italian motor with TOP LED Lights Life time warranty 1 year warranty to touch control panel Airflow 1250 m&Acirc;&sup3;/hr Hob Features :- Now first time in india, New IN burner Built-in Hob with SS frame Stand No need to cut out you kitchen slab Yes ! these Built-in Hobs can also be used as counter tops For using as Built-in Hob, just remove 4 legs &amp; SS frame and it&#39;s done No low flame, No Sim-off Italian gas valve for no sim-off Integrated auto ignition Extra strong MS pan support 8 mm thick toughened glass Italian Double ring burner - 2 big &amp; 2 small DIMENSIONS IN CM ( LXBXH) :-71X70X58</li>\r\n</ul>\r\n', '1'),
(18, '8', 'Puma Unisex Yacht Cvs Canvas Sneakers', '1,495.00 - 2,249.00', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.amazon.in/Puma-Unisex-Yacht-Canvas-Sneakers/dp/B00IWK796S/ref=sr_1_5?s=shoes&ie=UTF8&qid=1510379310&sr=1-5&nodeID=7386856031&psd=1&refinements=p_89%3APuma&dpID=4172Zb2GCRL&preST=_SX395_QL70_&dpSrc=srch', '', '', '24035_PumaSneaker.jpg', '<ul>\r\n	<li>Material: Canvas</li>\r\n	<li>Lifestyle: Casual</li>\r\n	<li>Closure Type: Slip On</li>\r\n	<li>Warranty Type: Manufacturer</li>\r\n	<li>Product warranty against manufacturing defects: 90 days</li>\r\n	<li>Care Instrutions: Allow your pair of shoes to air and de-odorize at regular basis; using a shoe-horn to wear your shoes will avoid damage to the back of your shoes; use shoe bags to prevent any stains or mildew</li>\r\n</ul>\r\n', '1'),
(19, '8', 'Catwalk Women\'s', '947.50', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.amazon.in/Catwalk-Womens-Black-UK-India/dp/B06XKCMJYP/ref=sr_1_58?s=shoes&ie=UTF8&qid=1510379753&sr=1-58&nodeID=1983633031&psd=1', '', '', '3173_catwalk_sandel.jpg', '<ul>\r\n	<li>Material Type: Synthetic</li>\r\n	<li>Lifestyle: Casual</li>\r\n	<li>Closure Type: Slip On</li>\r\n	<li>Heel Type: Western heel</li>\r\n	<li>Heel Height: 2.5 inches</li>\r\n	<li>Toe Style: Open Toe</li>\r\n	<li>Warranty Type: Manufacturer &amp; Seller</li>\r\n</ul>\r\n', '1'),
(21, '8', 'Barbie Girl\\\'s Fashion Sandals', '585.00', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.amazon.in/Barbie-Girls-Pink-Fashion-Sandals/dp/B01M16L788/ref=sr_1_7?s=shoes&rps=1&ie=UTF8&qid=1510379967&sr=1-7&nodeID=1983511031&psd=1&refinements=p_98%3A10440597031', '', '', '72926_BarbieGirl_Sandel.jpg', '<ul>\r\n	<li>Material Type: Synthetic</li>\r\n	<li>Lifestyle: Casual</li>\r\n	<li>Toe Style: Open Toe</li>\r\n	<li>No warranty</li>\r\n	<li>Care Instructions: Allow your pair of shoes to air and de-odorize at regular basis; use shoe bags to prevent any stains or mildew; dust any dry dirt from the surface using a clean cloth; do not use polish or shiner</li>\r\n</ul>\r\n', '1'),
(22, '7', 'Senco Gold 22k (916) Yellow Gold Pendant', '105,697.00', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.amazon.in/Senco-Gold-22k-Yellow-Pendant/dp/B01N6P020A/ref=sr_1_14?s=jewelry&ie=UTF8&qid=1510380716&sr=1-14&nodeID=5210069031&psd=1', '', '', '47959_GoldPendant.jpg', '<ul>\r\n	<li>This product can be returned in 10 days. Exchange and Buyback may be available offline as per the seller/brand policy</li>\r\n	<li>This pendant is made from 22k gold</li>\r\n	<li>This is a hand made product</li>\r\n	<li>Traditional temple pendant</li>\r\n	<li>The product is set in 22k gold certified by BIS hallmark</li>\r\n	<li>BIS hallmark is not a separate certificate, it is an inscription made on the product</li>\r\n	<li>The item will be delivered to you in a tamperproof package. Please inspect the package for any tampering before accepting the delivery</li>\r\n</ul>\r\n', '1'),
(23, '7', 'Nishtaa 22K Yellow Gold Drop Earrings', '15,884.00', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.amazon.in/Nishtaa-Yellow-Gold-Drop-Earrings/dp/B00NFD0TAU/ref=sr_1_41?s=jewelry&ie=UTF8&qid=1510381001&sr=1-41&nodeID=5210069031&psd=1', '', '', '19639_GoldEarings.jpg', '<ul>\r\n	<li>These earrings are made from 22k gold</li>\r\n	<li>The earrings have been adorned with cubic zirconia</li>\r\n	<li>This product is coming with certificate of authenticity, and is set in 22 karat gold certified by BIS</li>\r\n	<li>All diamonds are warranted to be conflict free</li>\r\n	<li>There is no buyback or exchange policy (replacement only under manufacturing defect)</li>\r\n	<li>BIS Hallmark is NOT a separate certificate. It is an inscription made on the product</li>\r\n	<li>The item will be delivered to you in a tamperproof package. Please inspect the package for any tampering before accepting the delivery</li>\r\n</ul>\r\n', '1'),
(24, '7', 'Araanz Jewels 22k (916) Yellow Gold Stunning Royal Ring', '12,722.00 -    14,265.00', '123, abc street, XYZ building, City: XYZ - 123456', '1234567890', 'Abc@abc.com', 'https://www.amazon.in/Araanz-Jewels-Yellow-Stunning-Royal/dp/B01LXR2L8O/ref=sr_1_36?s=jewelry&ie=UTF8&qid=1510381151&sr=1-36&nodeID=2152567031&psd=1&refinements=p_n_feature_seven_browse-bin%3A2160570031%2Cp_n_pct-off-with-tax%3A5-', '', '', '45410_GoldRing.jpg', '<ul>\r\n	<li>This ring is made from 22k gold</li>\r\n	<li>Hallmarked handmade vintage and trendy designer rings for women that will always stay evergreen</li>\r\n	<li>3% GST</li>\r\n	<li>The item will be delivered to you in a tamperproof package. Please inspect the package for any tampering before accepting the delivery</li>\r\n</ul>\r\n', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_catalog_image`
--

CREATE TABLE `tbl_catalog_image` (
  `img_id` int(11) NOT NULL,
  `catalog_id` varchar(255) NOT NULL,
  `catalog_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_catalog_image`
--

INSERT INTO `tbl_catalog_image` (`img_id`, `catalog_id`, `catalog_image`) VALUES
(7, '3', '9421_mi-mi-a1-na-original-imaexcvyzg9yjubn.jpeg'),
(8, '3', '69523_mi-mi-a1-na-original-imaexg9ppespasmh.jpeg'),
(9, '3', '78933_mi-mi-a1-na-original-imaexg9zgxrxekgp.jpeg'),
(10, '3', '74864_mi-mi-a1-na-original-imaexg9zhehx8yfm.jpeg'),
(11, '3', '55574_mi-mi-a1-na-original-imaexg9zkqbgzbk4.jpeg'),
(12, '4', '21296_OnePlus5_1.jpg'),
(13, '4', '1908_OnePlus5_2.jpg'),
(14, '4', '94585_OnePlus5_3.jpg'),
(15, '4', '85386_OnePlus5_4.jpg'),
(16, '5', '55840_Nokia6_2.jpg'),
(17, '5', '74489_Nokia6_3.jpg'),
(18, '6', '10621_VivoV5Plus_1.jpg'),
(19, '6', '1308_VivoV5Plus_2.jpg'),
(20, '6', '71422_VivoV5Plus_3.jpg'),
(21, '7', '86534_aston-martin-db11_1.jpg'),
(22, '7', '99898_aston-martin-db11_2.jpg'),
(24, '8', '56815_BMW-i8_1.jpg'),
(25, '8', '90237_BMW-i8_2.jpg'),
(26, '9', '91744_Lamborghini-Aventador_1.jpg'),
(27, '9', '3993_Lamborghini-Aventador_2.jpg'),
(28, '10', '26523_USPOLO_2.jpg'),
(29, '11', '23551_RituKumar_casual_dress2.jpg'),
(30, '11', '77249_RituKumar_casual_dress3.jpg'),
(31, '11', '61290_RituKumar_casual_dress4.jpg'),
(32, '12', '72091_Marie_Dress1.jpeg'),
(33, '12', '75278_Marie_Dress2.jpeg'),
(34, '12', '48739_Marie_Dress3.jpeg'),
(35, '13', '31739_LGLED1.jpg'),
(36, '13', '44478_LGLED2.jpg'),
(39, '15', '44247_Samsung545L_1.jpg'),
(40, '15', '51104_Samsung545L_2.jpg'),
(41, '16', '62467_LG8KGW_1.jpg'),
(42, '16', '80888_LG8KGW_2.jpg'),
(43, '17', '93677_Chimneys_1.jpg'),
(44, '17', '35777_Chimneys_2.jpg'),
(45, '18', '47850_PumaSneaker1.jpg'),
(46, '18', '54690_PumaSneaker2.jpg'),
(47, '18', '96521_PumaSneaker3.jpg'),
(48, '19', '666_catwalk_sandel1.jpg'),
(49, '19', '66068_catwalk_sandel2.jpg'),
(50, '19', '37934_catwalk_sandel3.jpg'),
(54, '21', '57967_BarbieGirl_Sandel1.jpg'),
(55, '21', '76286_BarbieGirl_Sandel2.jpg'),
(56, '21', '82978_BarbieGirl_Sandel3.jpg'),
(57, '22', '86530_GoldPendant1.jpg'),
(58, '22', '66687_GoldPendant2.jpg'),
(59, '23', '65384_GoldEarings1.jpg'),
(60, '23', '72339_GoldEarings2.jpg'),
(61, '24', '20718_GoldRing1.jpg'),
(62, '24', '39218_GoldRing2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `cid` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_image` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`cid`, `category_name`, `category_image`, `path`) VALUES
(2, 'Phones', '67655_DG27124_053017_ComplexGrid_EC_CellPhones.jpg', ''),
(3, 'Clothing', '6616_Clothing.jpg', ''),
(6, 'Electronics', '25982_Flipkartele..jpg', ''),
(7, 'Jewellery', '20645_jewellery.jpg', ''),
(8, 'Footwear', '76540_footwear.jpg', ''),
(9, 'Cars', '96373_car.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings`
--

CREATE TABLE `tbl_settings` (
  `id` int(11) NOT NULL,
  `app_name` varchar(255) NOT NULL,
  `app_logo` varchar(255) NOT NULL,
  `app_email` varchar(255) NOT NULL,
  `app_version` varchar(255) NOT NULL,
  `app_author` varchar(255) NOT NULL,
  `app_contact` varchar(255) NOT NULL,
  `app_website` varchar(255) NOT NULL,
  `app_description` text NOT NULL,
  `app_developed_by` varchar(255) NOT NULL,
  `app_privacy_policy` text NOT NULL,
  `api_latest_limit` int(3) NOT NULL,
  `api_cat_order_by` varchar(255) NOT NULL,
  `api_cat_post_order_by` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_settings` (`id`, `app_name`, `app_logo`, `app_email`, `app_version`, `app_author`, `app_contact`, `app_website`, `app_description`, `app_developed_by`, `app_privacy_policy`, `api_latest_limit`, `api_cat_order_by`, `api_cat_post_order_by`) VALUES
(1, 'Catalog App', 'Icon_144.png', 'info@viaviweb.com', '1.0.0', 'viaviwebtech', '+91 1234567891', 'www.viaviweb.com', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 'Viavi Webtech', '<p><strong>We are committed to protecting your privacy</strong></p>\n\n<p>We collect the minimum amount of information about you that is commensurate with providing you with a satisfactory service. This policy indicates the type of processes that may result in data being collected about you. Your use of this website gives us the right to collect that information.&nbsp;</p>\n\n<p><strong>Information Collected</strong></p>\n\n<p>We may collect any or all of the information that you give us depending on the type of transaction you enter into, including your name, address, telephone number, and email address, together with data about your use of the website. Other information that may be needed from time to time to process a request may also be collected as indicated on the website.</p>\n\n<p><strong>Information Use</strong></p>\n\n<p>We use the information collected primarily to process the task for which you visited the website. Data collected in the UK is held in accordance with the Data Protection Act. All reasonable precautions are taken to prevent unauthorised access to this information. This safeguard may require you to provide additional forms of identity should you wish to obtain information about your account details.</p>\n\n<p><strong>Cookies</strong></p>\n\n<p>Your Internet browser has the in-built facility for storing small files - &quot;cookies&quot; - that hold information which allows a website to recognise your account. Our website takes advantage of this facility to enhance your experience. You have the ability to prevent your computer from accepting cookies but, if you do, certain functionality on the website may be impaired.</p>\n\n<p><strong>Disclosing Information</strong></p>\n\n<p>We do not disclose any personal information obtained about you from this website to third parties unless you permit us to do so by ticking the relevant boxes in registration or competition forms. We may also use the information to keep in contact with you and inform you of developments associated with us. You will be given the opportunity to remove yourself from any mailing list or similar device. If at any time in the future we should wish to disclose information collected on this website to any third party, it would only be with your knowledge and consent.&nbsp;</p>\n\n<p>We may from time to time provide information of a general nature to third parties - for example, the number of individuals visiting our website or completing a registration form, but we will not use any information that could identify those individuals.&nbsp;</p>\n\n<p>In addition Dummy may work with third parties for the purpose of delivering targeted behavioural advertising to the Dummy website. Through the use of cookies, anonymous information about your use of our websites and other websites will be used to provide more relevant adverts about goods and services of interest to you. For more information on online behavioural advertising and about how to turn this feature off, please visit youronlinechoices.com/opt-out.</p>\n\n<p><strong>Changes to this Policy</strong></p>\n\n<p>Any changes to our Privacy Policy will be placed here and will supersede this version of our policy. We will take reasonable steps to draw your attention to any changes in our policy. However, to be on the safe side, we suggest that you read this document each time you use the website to ensure that it still meets with your approval.</p>\n\n<p><strong>Contacting Us</strong></p>\n\n<p>If you have any questions about our Privacy Policy, or if you want to know what information we have collected about you, please email us at hd@dummy.com. You can also correct any factual errors in that information or require us to remove your details form any list under our control.</p>\n', 15, 'category_name', 'DESC');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_catalog`
--
ALTER TABLE `tbl_catalog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_catalog_image`
--
ALTER TABLE `tbl_catalog_image`
  ADD PRIMARY KEY (`img_id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_catalog`
--
ALTER TABLE `tbl_catalog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tbl_catalog_image`
--
ALTER TABLE `tbl_catalog_image`
  MODIFY `img_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
