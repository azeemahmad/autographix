<?php include("includes/header.php");

  require("includes/function.php");
  require("language/language.php");

  require_once("thumbnail_images.class.php");

  //All Category
  $cat_qry="SELECT * FROM tbl_category ORDER BY category_name";
  $cat_result=mysqli_query($mysqli,$cat_qry);
 
  if(isset($_POST['submit']))
  {
      

     $file_name= str_replace(" ","-",$_FILES['catalog_main_image']['name']);

     $catalog_main_image=rand(0,99999)."_".$file_name;
       
     //Main Image
     $tpath1='images/'.$catalog_main_image;       
     $pic1=compress_image($_FILES["catalog_main_image"]["tmp_name"], $tpath1, 80);
   
        
       $data = array( 
         'cat_id'  =>  $_POST['cat_id'],
         'catalog_name'  =>  addslashes($_POST['catalog_name']),
         'catalog_price'  =>  $_POST['catalog_price'],
         'catalog_address'  =>  addslashes($_POST['catalog_address']),
         'catalog_tel_no'  =>  $_POST['catalog_tel_no'],
         'catalog_email'  =>  $_POST['catalog_email'],
         'catalog_wesite'  =>  $_POST['catalog_wesite'],
         'catalog_map_latitude'  =>  $_POST['catalog_map_latitude'],
         'catalog_map_longitude'  =>  $_POST['catalog_map_longitude'],
         'catalog_main_image'  =>  $catalog_main_image,           
         'catalog_desc'  =>  addslashes($_POST['catalog_desc'])
          );    

    $qry = Insert('tbl_catalog',$data);  

    $catalog_id=mysqli_insert_id($mysqli);

   $size_sum = array_sum($_FILES['catalog_image']['size']);
     
  if($size_sum > 0)
   { 
      for ($i = 0; $i < count($_FILES['catalog_image']['name']); $i++) 
      {
           $file_name= str_replace(" ","-",$_FILES['catalog_image']['name'][$i]);
             
           $catalog_image=rand(0,99999)."_".$file_name;
         
           //Main Image
           $tpath1='images/'.$catalog_image;       
           $pic1=compress_image($_FILES["catalog_image"]["tmp_name"][$i], $tpath1, 80);

            $data1 = array(
                'catalog_id'=>$catalog_id,
                'catalog_image'  => $catalog_image                         
                );      

            $qry1 = Insert('tbl_catalog_image',$data1); 

      }
    }

      
    $_SESSION['msg']="10";
 
    header( "Location:add_catalog.php");
    exit; 

    
  }
   

?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

 
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="page_title_block">
            <div class="col-md-5 col-xs-12">
              <div class="page_title">Add Catalog</div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row mrg-top">
            <div class="col-md-12">
               
              <div class="col-md-12 col-sm-12">
                <?php if(isset($_SESSION['msg'])){?> 
                 <div class="alert alert-success alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                  <?php echo $client_lang[$_SESSION['msg']] ; ?></a> </div>
                <?php unset($_SESSION['msg']);}?> 
              </div>
            </div>
          </div>
          <div class="card-body mrg_bottom"> 
            <form action="" name="addeditcategory" method="post" class="form form-horizontal" enctype="multipart/form-data">
 
              <div class="section">
                <div class="section-body">
                 
                  
                  <div class="form-group">
                    <label class="col-md-3 control-label">Category :-</label>
                    <div class="col-md-6">
                      <select name="cat_id" id="cat_id" class="select2" required>
                        <option value="">--Select Category--</option>
                        <?php
                            while($cat_row=mysqli_fetch_array($cat_result))
                            {
                        ?>                       
                        <option value="<?php echo $cat_row['cid'];?>"><?php echo $cat_row['category_name'];?></option>                           
                        <?php
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Catalog Name :-</label>
                    <div class="col-md-6">
                      <input type="text" name="catalog_name" id="catalog_name" value="" class="form-control" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Price :-</label>
                    <div class="col-md-6">
                      <input type="text" name="catalog_price" id="catalog_price" value="" class="form-control" required>
                    </div>
                  </div>                  
                  <div class="form-group">
                    <label class="col-md-3 control-label">Phone :-</label>
                    <div class="col-md-6">
                 
                      <input type="text" name="catalog_tel_no" id="catalog_tel_no" value="" class="form-control" required>

                     </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Email :-</label>
                    <div class="col-md-6">
                 
                      <input type="text" name="catalog_email" id="catalog_email" value="" class="form-control" required>

                     </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Website :-</label>
                    <div class="col-md-6">
                 
                      <input type="text" name="catalog_wesite" id="catalog_wesite" value="" class="form-control" required>

                     </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Address :-</label>
                    <div class="col-md-6">
                 
                      <textarea name="catalog_address" id="catalog_address" class="form-control"></textarea>

                     </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Latitude :-</label>
                    <div class="col-md-6">
                      <input type="text" name="catalog_map_latitude" id="catalog_map_latitude" value="" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Longitude :-</label>
                    <div class="col-md-6">
                      <input type="text" name="catalog_map_longitude" id="catalog_map_longitude" value="" class="form-control">
                    </div>
                  </div>
                   <div class="form-group">
                    <label class="col-md-3 control-label">&nbsp;</label>
                    <div class="col-md-6">
                      Get Latitude and Longitude <a href="http://www.latlong.net" target="_blank">Here!</a>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Featured Image :-</label>
                    <div class="col-md-6">
                      <div class="fileupload_block">
                        <input type="file" name="catalog_main_image" value="" id="fileupload">
                            
                            <div class="fileupload_img"><img type="image" src="assets/images/add-image.png" alt="Featured image" /></div>
                           
                      </div>
                    </div>
                  </div>                   
                  <div class="form-group">
                    <label class="col-md-3 control-label">Description :-</label>
                    <div class="col-md-6">
                 
                      <textarea name="catalog_desc" id="catalog_desc" class="form-control"></textarea>

                      <script>CKEDITOR.replace( 'catalog_desc' );</script>
                    </div>
                  </div>                   
                  <div class="form-group">&nbsp;</div>
                   <div class="form-group" id="image_news">
                    <label class="col-md-3 control-label">Gallery Image :-</label>
                    <div class="col-md-6">
                      <div class="fileupload_block">
                        <input type="file" name="catalog_image[]" value="" id="fileupload" multiple>
                            
                            <div class="fileupload_img"><img type="image" src="assets/images/add-image.png" alt="Featured image" /></div>
                           
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
        
<?php include("includes/footer.php");?>       
