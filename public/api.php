<?php include("includes/connection.php");
 	  include("includes/function.php"); 	
	
	$file_path = 'http://'.$_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']).'/';
 	 
	if(isset($_GET['cat_list']))
 	{
 		$jsonObj= array();
		
		$cat_order=API_CAT_ORDER_BY;


		$query="SELECT cid,category_name,category_image FROM tbl_category ORDER BY tbl_category.".$cat_order."";
		$sql = mysqli_query($mysqli,$query)or die(mysql_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			 

			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
 
			array_push($jsonObj,$row);
		
		}

		$set['CATALOG_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();
 	}
	else if(isset($_GET['cat_id']))
	{
		$post_order_by=API_CAT_POST_ORDER_BY;

		$cat_id=$_GET['cat_id'];	

		$jsonObj= array();	
	
	    $query="SELECT * FROM tbl_catalog
		LEFT JOIN tbl_category ON tbl_catalog.cat_id= tbl_category.cid 
		where tbl_catalog.cat_id='".$cat_id."' AND tbl_catalog.status='1' ORDER BY tbl_catalog.id ".$post_order_by."";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row['id'] = $data['id'];
			$row['cat_id'] = $data['cat_id'];
 			$row['catalog_name'] = stripslashes($data['catalog_name']);
			$row['catalog_price'] = $data['catalog_price'];
			$row['catalog_tel_no'] = $data['catalog_tel_no'];
			$row['catalog_email'] = $data['catalog_email'];
			$row['catalog_wesite'] = $data['catalog_wesite'];
			$row['catalog_address'] = stripslashes($data['catalog_address']);
			$row['catalog_map_latitude'] = $data['catalog_map_latitude'];
			$row['catalog_map_longitude'] = $data['catalog_map_longitude'];
 
			$row['catalog_main_image'] = $file_path.'images/'.$data['catalog_main_image'];
 			$row['catalog_desc'] = stripslashes($data['catalog_desc']);  
 
			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
			 

			array_push($jsonObj,$row);
		
		}

		$set['CATALOG_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

		
	}
	else if(isset($_GET['latest']))
	{
		$limit=API_LATEST_LIMIT;
 
		$jsonObj= array();	
	
	    $query="SELECT * FROM tbl_catalog
		LEFT JOIN tbl_category ON tbl_catalog.cat_id= tbl_category.cid 
		WHERE tbl_catalog.status='1' ORDER BY tbl_catalog.id DESC LIMIT $limit";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row['id'] = $data['id'];
			$row['cat_id'] = $data['cat_id'];
 			$row['catalog_name'] = stripslashes($data['catalog_name']);
			$row['catalog_price'] = $data['catalog_price'];
			$row['catalog_tel_no'] = $data['catalog_tel_no'];
			$row['catalog_email'] = $data['catalog_email'];
			$row['catalog_wesite'] = $data['catalog_wesite'];
			$row['catalog_address'] = stripslashes($data['catalog_address']);
			$row['catalog_map_latitude'] = $data['catalog_map_latitude'];
			$row['catalog_map_longitude'] = $data['catalog_map_longitude'];
 
			$row['catalog_main_image'] = $file_path.'images/'.$data['catalog_main_image'];
 			$row['catalog_desc'] = stripslashes($data['catalog_desc']);  
 
			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
			 

			array_push($jsonObj,$row);
		
		}

		$set['CATALOG_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

		
	}	  
	else if(isset($_GET['catalog_id']))
	{ 
				 
		$jsonObj= array();	

		$query="SELECT * FROM tbl_catalog
		LEFT JOIN tbl_category ON tbl_catalog.cat_id= tbl_category.cid
		WHERE tbl_catalog.id='".$_GET['catalog_id']."'";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row['id'] = $data['id'];
			$row['cat_id'] = $data['cat_id'];
 			$row['catalog_name'] = stripslashes($data['catalog_name']);
			$row['catalog_price'] = $data['catalog_price'];
			$row['catalog_tel_no'] = $data['catalog_tel_no'];
			$row['catalog_email'] = $data['catalog_email'];
			$row['catalog_wesite'] = $data['catalog_wesite'];
			$row['catalog_address'] = stripslashes($data['catalog_address']);
			$row['catalog_map_latitude'] = $data['catalog_map_latitude'];
			$row['catalog_map_longitude'] = $data['catalog_map_longitude'];
 
			$row['catalog_main_image'] = $file_path.'images/'.$data['catalog_main_image'];
 			$row['catalog_desc'] = stripslashes($data['catalog_desc']);  

 			//Gallery Images
		      $qry1="SELECT * FROM tbl_catalog_image WHERE catalog_id='".$_GET['catalog_id']."'";
		      $result1=mysqli_query($mysqli,$qry1); 

		      if($result1->num_rows > 0)
		      {
		      		while ($row_img=mysqli_fetch_array($result1)) {
 		      	
		 		      	$row1['catalog_image'] = $file_path.'images/'.$row_img['catalog_image'];

		 		      	$row['catalog_image_list'][]= $row1;
				      }
		     
		      }
		      else
		      {	
		      		 
		      		$row['catalog_image_list'][]= '';
		      }
 
			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
 
			array_push($jsonObj,$row);
		
		}
 

		$set['CATALOG_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();	
 

	}	  	 
	else 
	{
		$jsonObj= array();	

		$query="SELECT * FROM tbl_settings WHERE id='1'";
		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			 
			$row['app_name'] = $data['app_name'];
			$row['app_logo'] = $data['app_logo'];
			$row['app_version'] = $data['app_version'];
			$row['app_author'] = $data['app_author'];
			$row['app_contact'] = $data['app_contact'];
			$row['app_email'] = $data['app_email'];
			$row['app_website'] = $data['app_website'];
			$row['app_description'] = stripslashes($data['app_description']);
 			$row['app_developed_by'] = $data['app_developed_by'];

			$row['app_privacy_policy'] = stripslashes($data['app_privacy_policy']);
	

			array_push($jsonObj,$row);
		
		}

		$set['CATALOG_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();	
	}		
	 
	 
?>