<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;


class ApiUrlsController extends Controller
{

public function index()
{
    $settings_row=Setting::first();
    return view('api_urls',compact('settings_row'));

}

}
