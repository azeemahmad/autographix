<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;


class SettingController extends Controller
{




    public function index()
    {
        $settings_row=Setting::first();
        return view('setting',compact('settings_row'));

    }

    public function app_setting(Request $request,$id){
        $requestData=$request->all();
        if ($request->hasFile('app_logo')) {
            $filename = $this->getFileName($request->app_logo);
            $request->app_logo->move(base_path('public/images/'), $filename);
            $requestData['app_logo']=$filename;
        }
        $settings_row=Setting::find($id);
        $settings_row->update($requestData);
        return redirect('setting')->with('flash_message', 'APP Setting Updated!');

    }
    public function api_setting(Request $request,$id){
        $requestData=$request->all();
        $settings_row=Setting::find($id);
        $settings_row->update($requestData);
        return redirect('setting')->with('flash_message', 'API Setting Updated!');

    }
    public function api_privacy_policy(Request $request,$id){
        $requestData=$request->all();
        $settings_row=Setting::find($id);
        $settings_row->update($requestData);
        return redirect('setting')->with('flash_message', 'Api Privacy Policy Setting Updated!');

    }

    protected function getFileName($file)
    {
        return str_random(32) . '.' . $file->extension();
    }

}
