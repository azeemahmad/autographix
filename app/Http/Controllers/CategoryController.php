<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;


class CategoryController extends Controller
{




    public function index()
    {
        $category=Category::get();
        return view('category',compact('category'));

    }
    public function create()
    {

        return view('add_category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'category_name' => 'required|string',
            'category_image'=> 'required|image|mimes:jpeg,jpg,png',
        ]);
        $requestData = $request->all();
        if ($request->hasFile('category_image')) {
            $filename = $this->getFileName($request->category_image);
            $request->category_image->move(base_path('public/images/thumbs/'), $filename);
            $requestData['category_image']=$filename;
        }
        Category::create($requestData);

        return redirect('category')->with('flash_message', 'Category added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);

        return view('add_category', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_name' => 'required|string',
        ]);
        $requestData = $request->all();
        if ($request->hasFile('category_image')) {
            $this->validate($request, [
             'category_image'=> 'required|image|mimes:jpeg,jpg,png'
            ]);
            $filename = $this->getFileName($request->category_image);
            $request->category_image->move(base_path('public/images/thumbs/'), $filename);
            $requestData['category_image']=$filename;
        }
        $category = Category::findOrFail($id);
        $category->update($requestData);

        return redirect('category')->with('flash_message', 'Category updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Category::destroy($id);

        return redirect('category')->with('flash_message', 'Category deleted!');
    }

    protected function getFileName($file)
    {
        return str_random(32) . '.' . $file->extension();
    }

}
