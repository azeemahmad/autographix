<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Catalog;
use App\Models\Category;
use App\Models\CatalogImage;


class CatalogController extends Controller
{

    public function index()
    {
        $catalog=Catalog::paginate(10);
        return view('catalog',compact('catalog'));

    }
    public function create()
    {
        $category=Category::pluck('category_name','cid');
        return view('add_catalog',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'cat_id' => 'required',
            'catalog_main_image'=> 'required|image|mimes:jpeg,jpg,png',
            'catalog_name' => 'required',
            'catalog_price' => 'required',
            'catalog_tel_no' => 'required',
            'catalog_email' => 'required',
            'catalog_wesite' => 'required',
            'catalog_address' => 'required',
            'catalog_map_latitude' => 'required',
            'catalog_map_longitude' => 'required',
            'catalog_desc' => 'required',

        ]);
        $requestData = $request->all();
        if ($request->hasFile('catalog_main_image')) {
            $filename = $this->getFileName($request->catalog_main_image);
            $request->catalog_main_image->move(base_path('public/images/'), $filename);
            $requestData['catalog_main_image']=$filename;
        }
        $catalogDetails=Catalog::create($requestData);
        if ($files =$request->hasFile('catalog_image')) {
            foreach ($request->catalog_image as $file) {
                $fileImagename = $this->getFileName($file);
                $file->move(base_path('public/images/'), $fileImagename);
                $ImageData['catalog_id'] =  $catalogDetails->id;
                $ImageData['catalog_image'] = $fileImagename;
                CatalogImage::create($ImageData);
            }
        }
        return redirect('catalog')->with('flash_message', 'Catalog added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $category=Category::pluck('category_name','cid');
        $catalog = Catalog::findOrFail($id);
        $catlogImages=$catalog->catlogImage()->get();
        return view('add_catalog', compact('catalog','category','catlogImages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'cat_id' => 'required',
            'catalog_name' => 'required',
            'catalog_price' => 'required',
            'catalog_tel_no' => 'required',
            'catalog_email' => 'required',
            'catalog_wesite' => 'required',
            'catalog_address' => 'required',
            'catalog_map_latitude' => 'required',
            'catalog_map_longitude' => 'required',
            'catalog_desc' => 'required',

        ]);
        $requestData = $request->all();
        if ($request->hasFile('catalog_main_image')) {
            $this->validate($request, [
                'catalog_main_image'=> 'required|image|mimes:jpeg,jpg,png',
            ]);
            $filename = $this->getFileName($request->catalog_main_image);
            $request->catalog_main_image->move(base_path('public/images/'), $filename);
            $requestData['catalog_main_image']=$filename;
        }
        $catalogDetails = Catalog::findOrFail($id);
        $catalogDetails->update($requestData);
        if ($files =$request->hasFile('catalog_image')) {
            foreach ($request->catalog_image as $file) {
                $fileImagename = $this->getFileName($file);
                $file->move(base_path('public/images/'), $fileImagename);
                $ImageData['catalog_id'] =  $catalogDetails->id;
                $ImageData['catalog_image'] = $fileImagename;
                CatalogImage::create($ImageData);
            }
        }

        return redirect('catalog')->with('flash_message', 'Catalog updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $catalog = Catalog::findOrFail($id);
        $catalog->catlogImage()->delete();
        Catalog::destroy($id);

        return redirect('catalog')->with('flash_message', 'Catalog deleted!');
    }

    protected function getFileName($file)
    {
        return str_random(32) . '.' . $file->extension();
    }

}
