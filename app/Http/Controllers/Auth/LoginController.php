<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected function guard()
    {
        return Auth::guard('web');
    }
    protected $redirectTo = '/home';
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm(){
        return view('auth.login');
    }
    public function postlogin(Request $request)
    {

        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);
        $remember_me = $request->has('remember') ? true : false;
        $user = User::where('username', $request->username)->first();

            if (!$user) {
                session()->flash('error_message', 'Username is not registered with us');
                return redirect('/login');

            } else if (Auth::guard('web')->attempt(['username' => $request->username, 'password' => $request->password], $remember_me)) {
                $this->guard()->login($user);
                return redirect($this->redirectTo);
            } else {
                return redirect('/login')->with('error_message','Incorrect Password');

            }

    }
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        return redirect('/login')->with('flash_message', 'Logout Successfully !');
    }
}
