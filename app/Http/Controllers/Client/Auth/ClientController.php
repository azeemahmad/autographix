<?php

namespace App\Http\Controllers\Client\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\Validator;



class ClientController extends Controller
{

    public function send_sms($number,$message){


        // $user = 'TRANSAC1';
        // $key = 'cafbae387eXX';
        // $message = urlencode ($message);
        // $senderid = 'INFOSM';
        // $accusage = 1;
        // $url="pt.k9media.in/submitsms.jsp?user=".$user."&key=".$key."&mobile=".$number."&senderid=".$senderid."&accusage=".$accusage."&message=".$message."";

        /* MSG 91 Creds */
        $authkey="273923ATTMp8RVanSe5cc18868";
        $senderid = 'STKFLX';
        $message = urlencode ($message);

        $url="http://api.msg91.com/api/sendhttp.php?route=4&sender=".$senderid."&mobiles=".$number."&authkey=".$authkey."&message=".$message."&country=91";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($ch);
        curl_close($ch);
        if($curl_response)
        {
            return  true;
        }
        else
        {
            return  false;
        }
    }

    public function send_notification($message,$link){

        $url = 'https://cp.pushwoosh.com/json/1.3/createMessage';
        $custom_data = array('message'=>$message,"link"=>$link);
        $data=array(
            'application' => '9881F-21393',
            'auth' => 'zWxPbA4wdy6P8MJejiGlM5qe1j6kIxm6tkPiv5gPFeqTCAOYspyEYr84Cu9gi41bgnGD2Bac6enEsf4ksDT2',
            'notifications' => array(
                array(
                    'send_date' => 'now',
                    'content' => $message,
                    'data' => array('custom' => $custom_data),
                )
            )
        );
        $request = json_encode(['request' => $data]);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        return $response;

    }

    // public function send_noti(Request $req){
    //     $message = $req->message;
    //     $url = 'https://cp.pushwoosh.com/json/1.3/createMessage';

    //     $data=array(
    //                 'application' => '9881F-21393',
    //                 'auth' => 'zWxPbA4wdy6P8MJejiGlM5qe1j6kIxm6tkPiv5gPFeqTCAOYspyEYr84Cu9gi41bgnGD2Bac6enEsf4ksDT2',
    //                 'notifications' => array(
    //                         array(
    //                             'send_date' => 'now',
    //                             'content' => $message,
    //                             'data' => array('custom' => ''),
    //                             'link' => 'http://pushwoosh.com/'
    //                         )
    //                     )
    //                 );
    //     $request = json_encode(['request' => $data]);

    //     $ch = curl_init($url);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    //     curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
    //     curl_setopt($ch, CURLOPT_HEADER, true);
    //     curl_setopt($ch, CURLOPT_POST, true);
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
    //     $response = curl_exec($ch);
    //     $info = curl_getinfo($ch);
    //     curl_close($ch);
    //     echo $response;die;

    // }

    public function mobile_verification(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'mobile' => 'required|digits:10',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $data=$request->all();

        $user=Client::where('mobile',$data['mobile'])->first();
        if(!$user){
            $userdata['mobile']=$data['mobile'];
            $user=Client::create($userdata);
        }
        $otp = rand(1000, 9999);

        /************Write SMS Sending code *************/
        $message= 'Your verification passcode is '.$otp;

        ///////////write code////////////
        /*************End SMS code ******************/
        $user->otp=$otp;
        $user->mobile_verified_status=0;
        $user->mobile_verified_status=date('Y-m-d H:i');
        $client =$user->save();
        if($client){
            $sms_response = $this->send_sms($data['mobile'],$message);
            if($sms_response){
                $response = array("code"=>200,"data"=>null,"message"=>"Otp sent successfully.");
            }else{
                $response = array("code"=>400,"data"=>null,"message"=>"Otp sending fail. Please try again");
            }
            return json_encode($response);
        }
        else{
            $response = array("code"=>400,"data"=>null,"message"=>"Otp sending fail. Please try again");
            return json_encode($response);
        }

    }

    public function otp_verification(Request $request)
    {
        $data=$request->all();
        $user=Client::where('mobile',$data['mobile'])->first();
        if(!$user){
            $response = array("code"=>401,"data"=>null,"message"=>"Invalid User.");
            return json_encode($response);
        }
        elseif($data['otp'] != $user->otp ){
            $response = array("code"=>401,"data"=>null,"message"=>"Wrong OTP.");
            return json_encode($response);
        }
        else{

            $user->mobile_verified_status=1;
            $user->mobile_verified_at=date('Y-m-d h:i:s');
            if(!empty($user->pushtoken) && !is_null($user->pushtoken) && $user->pushtoken!=''){
                $pushtoken=$user->pushtoken.','.$data['pushtoken'];
            }else{
                $pushtoken=$data['pushtoken'];
            }
            $user->pushtoken=$pushtoken;
            //$user->credits=100;
            $user->save();
            $response = array("code"=>200,"data"=>$user,"message"=>"Otp verified successfully.");
            return json_encode($response);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'mobile'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $user=Client::where('mobile',$input['mobile'])->first();
        if($user){
            $user->name= $input['firstname'].' '.$input['lastname'];
            $user->profile_picture = $input['profile_picture'];
            $user->save();
            $token = $user->createToken('access_token')->accessToken;
            $user->access_token=$token;
            $user->save();
            $response = array("code"=>200,"data"=>$user,"message"=>"Successfully Register.");
            return json_encode($response);
        }
        else{
            $response = array("code"=>401,"data"=>null,"message"=>"Invalid User.");
            return json_encode($response);
        }


    }

    public function update_account_details(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'user_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $user=Client::where('id',$input['user_id'])->first();
        if($user){
            $user->name= $input['firstname'].' '.$input['lastname'];
            $user->profile_picture = $input['profile_picture'];
            $user->save();
            $response = array("code"=>200,"data"=>$user,"message"=>"Successfully Register.");
            return json_encode($response);
        }
        else{
            $response = array("code"=>401,"data"=>null,"message"=>"Invalid User.");
            return json_encode($response);
        }


    }

    public function get_account_details(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $user_details=Client::where('id',$request->user_id)->first();

        $user=array();
        if($user_details){
            $user['name'] = $user_details->name;
            $user['profile_picture'] = $user_details->profile_picture;
            $user['mobile'] = $user_details->mobile;
            $user['credits'] = $user_details->credits;
            $response = array("code"=>200,"data"=>$user,"message"=>"Success");
            return json_encode($response);
        }else{
            $response = array("code"=>401,"data"=>null,"message"=>"Invalid User.");
            return json_encode($response);
        }
    }
}
