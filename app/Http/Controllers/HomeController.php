<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Catalog;
use App\Models\Setting;
use App\Models\CatalogImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $total_category=Category::count();
        $total_catalog=Catalog::count();
        return view('home',compact('total_category','total_catalog'));
    }
    public function profile()
    {
        $user=Auth::user();
        return view('profile',compact('user'));
    }
    public function saveprofile(Request $request){

        $user_list=Auth::user();
        if ($request['password'] == null && $request['password_confirmation'] == null && $request['old_password'] == null) {
            $this->validate($request, [
                'username' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,' . Auth::user()->id,
            ]);
        } else {
            if (!Hash::check($request['old_password'], Auth::user()->password)) {
                return redirect()->back()->with('error_password', 'Old Password not Match from Database!');
            }

            $this->validate($request, [
                'username' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,' . Auth::user()->id,
                'password' => 'required|string|min:6|confirmed|different:old_password',
                'old_password' => 'required|string|min:6'
            ]);
        }
        $data=$request->all();
        if($request['password'] == null && $request['password_confirmation'] == null && $request['old_password'] == null) {
            if ($request->hasFile('image')) {
                $filename = $this->getFileName($request->image);
                $request->image->move(base_path('public/images'), $filename);

                $user_list->update([
                    'username' => $data['username'],
                    'email' => $data['email'],
                    'image' => $filename
                ]);
            } else {
                $user_list->update([
                    'username' => $data['username'],
                    'email' => $data['email'],
                ]);
            }

        } else {

            if ($request->hasFile('image')) {
                $filename = $this->getFileName($request->image);
                $request->image->move(base_path('public/images'), $filename);

                $user_list->update([
                    'username' => $data['username'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'image' => $filename
                ]);

            } else {
                $user_list->update([
                    'username' => $data['username'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password'])
                ]);
            }
        }
        return redirect('profile')->with('flash_message', 'Profile updated successfully!');
    }

}
