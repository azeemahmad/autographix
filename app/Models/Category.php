<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Category extends Model
{

    protected $table = 'tbl_category';
    protected $primaryKey = 'cid';
    public $timestamps = false;
    protected $fillable = ['cid','category_name', 'category_image', 'path'];



}
