<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Setting extends Model
{

    protected $table = 'tbl_settings';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['app_name', 'app_logo', 'app_email','app_version','app_author','app_contact','app_website','app_description','app_developed_by','app_privacy_policy','api_latest_limit','api_cat_order_by','api_cat_post_order_by'];




}
