<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Catalog extends Model
{

    protected $table = 'tbl_catalog';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['cat_id', 'catalog_name', 'catalog_price','catalog_address','catalog_tel_no','catalog_email','catalog_wesite','catalog_map_latitude','catalog_map_longitude','catalog_main_image','catalog_desc','status'];


    public function category(){
        return $this->belongsTo('App\Models\Category','cat_id','cid');
    }
    public function catlogImage(){
        return $this->hasMany('App\Models\CatalogImage','catalog_id');
    }



}
