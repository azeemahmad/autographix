<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class CatalogImage extends Model
{

    protected $table = 'tbl_catalog_image';
    protected $primaryKey = 'img_id';
    public $timestamps = false;
    protected $fillable = ['img_id','catalog_id', 'catalog_image'];



}
